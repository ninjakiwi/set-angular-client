# Set Game Angular Client

A alternate client for the set server [here](https://gitlab.com/iamam34/set-game), made to learn angular.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.7.

## Running the game

This is setup to start on port `4000`. The server is usually on port `4200` or `3001`.

Start the server following instructions in it's repository.

Start this client with `yarn run start`.
