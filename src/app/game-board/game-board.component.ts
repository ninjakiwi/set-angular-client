import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { GameServerService } from '../game-server.service';
import { ServerMessage } from '../server-message';

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.css'],
  animations: [
    trigger('cardState', [ //TODO
      state('normal', style("*")),
      state('selected', style("*")),
      state('locked', style("*")),
      state('locked-selected', style("*")),
    ]),
    trigger('wiggle', [
      transition('false => true', [
        animate('1s', keyframes([
          style({transform: 'rotate(9deg)'}),
          style({transform: 'rotate(-8deg)'}),
          style({transform: 'rotate(7deg)'}),
          style({transform: 'rotate(-6deg)'}),
          style({transform: 'rotate(5deg)'}),
          style({transform: 'rotate(-4deg)'}),
          style({transform: 'rotate(3deg)'}),
          style({transform: 'rotate(-2deg)'}),
          style({transform: 'rotate(1deg)'}),
          style({transform: 'rotate(0deg)'}),
        ]))
      ])
    ]),
    trigger('onAppearDisappear', [
      transition(':enter', [
        style({
          opacity: 0,
          transform: 'translateY(20px)',
          maxWidth: 0
        }),
        animate('200ms 200ms', style({
          maxWidth: 0
        })),
        animate('500ms', style({
          maxWidth: '*',
        })),
        animate('200ms', style({
          opacity: 0,
          transform: 'translateY(20px)',
        })),
        animate('200ms', style({
          opacity: 1,
          transform: 'translateY(0)',
        })),
      ]),
      transition(':leave', [
        style({
          opacity: 1,
          transform: 'translateY(0)',
          boxShadow: '*',
          maxWidth: '*',
        }),
        animate('500ms', style({
          opacity: 1,
          transform: 'translateY(-10px)',
        })),
        animate('500ms', style({
          opacity: 0,
          transform: 'translateY(-20px)',
          boxShadow: '1.5px 1.5px 1px 2px rgb(214, 214, 214)',
          maxWidth: '*',
        })),
        animate('500ms', style({
          maxWidth: '0px',
        })),
      ])
    ])
  ]
})
export class GameBoardComponent implements OnInit {
  public cardsInPlay: string[] = [];
  public answerSet: string[] = [];
  public candidateSet: string[] = [];
  public deniedSet: string[] = [];
  public lockScreen: boolean = false;
  public countDownDisplay: boolean = false;

  constructor(private gameServerService: GameServerService) { }

  ngOnInit(): void {
    this.gameServerService.stateChanged.subscribe(
      (data: ServerMessage) => this.handleData.bind(this)(data)
    );
  }

  private handleData(result: ServerMessage): void {
    if (result.data.candidateSet) {
      this.candidateSet = result.data.candidateSet;
      // Display or hide count down timer
      if (result.data.candidateSet.length === 1 && result.data.isYou) {
          this.countDownDisplay = true;
      } else if (result.data.candidateSet.length === 0) {
          this.countDownDisplay = false;
      }
    }
    if (result.data.cheat && 'use_cheats' in window) {
      this.answerSet = result.data.cheat;
    }

    if (result.type === 'new-game') {
      this.lockScreen = false;
    } else if (result.type === 'set-claim-attempt') {
      this.lockScreen = false;
        if (result.data.isCorrect) {
            // Animations happen automatically
        } else {
            this.deniedSet = result.data.attemptedSet;
        }
    } else if (result.type === 'card-selected') {
        this.lockScreen = !result.data.isYou;
    }
    if (result.data.cardsInPlay) {
        this.cardsInPlay = result.data.cardsInPlay;
    }
  }

  getAreaByIndex(index: number): string {
    return `${index % 3 + 1} / ${Math.floor(index / 3) + 1}`;
  }

  handleClickOnCard(cardDefinition: string): void {
    if (this.lockScreen || this.candidateSet.includes(cardDefinition)) { return; }
    this.gameServerService.sendMessage({
      type: 'card-select',
      data: {
        card: cardDefinition
      }
    })
  }

  handleAnimationEnd(cardDefinition: string): void {
    const cardDeniedIndex = this.deniedSet.indexOf(cardDefinition)
    if (cardDeniedIndex >= 0) {
        const newDeniedSet = [...this.deniedSet];
        newDeniedSet.splice(cardDeniedIndex, 1);
        this.deniedSet = newDeniedSet;
    }
  }

}
