import { Injectable, EventEmitter } from '@angular/core';
import { ServerMessage } from './server-message';

@Injectable({
  providedIn: 'root'
})
export class GameServerService {
  public stateChanged: EventEmitter<ServerMessage>;
  private ws: WebSocket;

  constructor() {
    const host = window.location.hostname;
    const port = 4200;

    this.stateChanged = new EventEmitter<ServerMessage>();

    this.ws = new WebSocket(`ws://${host}:${port}`);
    this.ws.onmessage = (data: any) => {
      const result = JSON.parse(data.data)
      // const result = JSON.parse(data as string);
      this.stateChanged.emit(result);
    };
  }

  sendMessage(message: any) {
    this.ws.send(JSON.stringify(message));
  }

  setUsername(name: string) {
    this.ws.send(JSON.stringify({
      type: "set-username",
      data: {
        username: name
      }
    }));
  }

  joinGame(game: string) {
    this.ws.send(JSON.stringify({
      type: "join-game",
      data: {
        gameId: game
      }
    }));
  }

  leaveGame() {
    this.ws.send(JSON.stringify({
      type: "leave-game"
    }));
  }

  restartGame() {
    this.ws.send(JSON.stringify({
      type: "reset-game"
    }));
  }
}
