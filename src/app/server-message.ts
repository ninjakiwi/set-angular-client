export interface ServerMessage {
    type: string,
    data: ServerData,
}

export interface ServerData {
    cardsInPlay: string[],
    candidateSet: string[],
    isYou: boolean,
    userId: string,
    scoreboard: ScoreboardData,
    cheat: string[],
    isCorrect: boolean,
    attemptedSet: string[],
    message: string,
    gameId: string,
    gameIds: string[],
    gameIsOver: boolean,
    usernames: {[key: string]: string},
    username: string,
}

export interface ScoreboardData {
    [key: string]: {
        mistakes: number,
        sets: [string[]]
    }
}
