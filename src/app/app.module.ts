import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { GameBoardComponent } from './game-board/game-board.component';
import { CardComponent } from './card/card.component';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { OverlayComponent } from './overlay/overlay.component';
import { FindGameComponent } from './find-game/find-game.component';
import { GameFinishedComponent } from './game-finished/game-finished.component';
import { PlacingPipe } from './placing.pipe';
import { SidebarComponent } from './sidebar/sidebar.component';

@NgModule({
  declarations: [
    AppComponent,
    GameBoardComponent,
    CardComponent,
    ScoreboardComponent,
    OverlayComponent,
    FindGameComponent,
    GameFinishedComponent,
    PlacingPipe,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
