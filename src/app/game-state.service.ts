import { Injectable } from '@angular/core';
import { GameServerService } from './game-server.service';
import { Score } from './score';
import { ScoreboardData, ServerMessage } from './server-message';

@Injectable({
  providedIn: 'root'
})
export class GameStateService {

  private _overlayLock: boolean = false;
  private gameId: string = "";
  private _allGameIds: string[] = [];
  private _userId: string = "";
  private _scores: Score[] = [];
  private _gameOverScores: Score[] = [];
  private gameStatus: string = "missing-game";
  private usernames: {[key: string]:string} = {};

  public get overlayLock() : boolean {
    return this._overlayLock;
  }

  public releaseOverlayLock() : void {
    this._overlayLock = false;
  }

  public get gameOverScores() : Score[] {
    return this._gameOverScores;
  }

  public get scores() : Score[] {
    return this._scores;
  }

  public get games() : string[] {
    return this._allGameIds;
  }

  public get userId() : string {
    return this._userId;
  }

  public get gameName() : string {
    return this.gameId;
  }

  constructor(private gameServerService: GameServerService) {
    gameServerService.stateChanged.subscribe(
      (data: ServerMessage) => this.handleData.bind(this)(data)
    );
  }

  handleData(result: ServerMessage): void {
    console.log(result)
    if (result.data.usernames) {
      this.usernames = result.data.usernames;
    }
    if (result.data.username) {
      this.usernames[result.data.userId] = result.data.username;
    }
    if (result.type === "game-left") {
      this.gameStatus = "missing-game"
      this.gameId = "";
    } else if (result.type === "new-game") {
      this.gameStatus = "playing-game"
    } else if (result.type === "game-joined") {
      this.gameStatus = "playing-game";
    }
    if (result.data.scoreboard) {
      this.updateScores(result.data.scoreboard);
    }
    if (result.data.userId) {
      this._userId = result.data.userId;
    }
    if (result.data.gameIds) {
      this._allGameIds = result.data.gameIds;
    }
    if (result.data.gameId) {
      this.gameId = result.data.gameId;
    }
    if (result.type === "game-over" || result.data.gameIsOver === true) {
      this.gameStatus = "game-over";
      this._gameOverScores = this.scores;
      this._overlayLock = true;
    }
  }

  private updateScores(new_scoreboard: ScoreboardData): void {
    let incomingScores: Score[] = [];
    for (const [userId, userScore] of Object.entries(new_scoreboard)) {
      const newUsername = userId in this.usernames ? this.usernames[userId] : userId.slice(0, 3);
      incomingScores.push({
        userId: userId,
        userName: newUsername,
        sets: userScore.sets,
        mistakes: userScore.mistakes
      });
    }
    incomingScores.sort((score1, score2) => {
      return score2.sets.length - score2.mistakes - score1.sets.length + score1.mistakes;
    });
    this._scores = incomingScores;
  }

  isInGame(): boolean {
    return this.gameId !== "";
  }

  getGameState(): string {
    if (this.overlayLock && this.isInGame() && this.gameStatus === 'playing-game') {
      return 'game-over'
    }
    return this.gameStatus;
  }

  getUserName():string {
    if (this.userId in this.usernames) {
      return this.usernames[this.userId];
    } else {
      return '';
    }
  }

}
