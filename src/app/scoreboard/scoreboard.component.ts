import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { GameServerService } from '../game-server.service';
import { GameStateService } from '../game-state.service';
import { ServerMessage } from '../server-message';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css'],
  animations: [
    trigger('onSetAttempt', [
      state('normal', style({
        opacity: 0,
        maxHeight: 0,
        padding: '0'
      })),
      state('hover', style({
        opacity: 0,
        maxHeight: 0,
        padding: '5px'
      })),
      transition('* => hover', animate('400ms ease')),
      transition(':enter', [
        style({
          opacity: 0,
          maxHeight: 0,
          padding: '0px'
        }),
        animate('200ms', style({
          opacity: 1,
          maxHeight: '400px',
          padding: '5px'
        })),
        animate('5000ms', style({
          opacity: 1,
          maxHeight: '400px',
          padding: '5px'
        })),
        animate('200ms', style({
          opacity: 0,
          maxHeight: 0,
          padding: '0px'
        })),
      ])
    ])
  ]
})
export class ScoreboardComponent implements OnInit {
  public currentAnimationState = 'normal'
  public justScoredSet: string = '';

  constructor(
    private gameServerService: GameServerService,
    public gameStateService: GameStateService
  ) { }

  ngOnInit(): void {
    this.gameServerService.stateChanged.subscribe((data: ServerMessage) => {
      if (data.type === 'set-claim-attempt' && data.data.isCorrect === true) {
        this.justScoredSet = data.data.attemptedSet.join('');
      }
    })
  }

  random(seed: string): number {
    // randomish number between -2 and 2
    let x = Math.sin(+seed) * 13373;
    x = x - Math.floor(x);
    return x * 4 - 2;
  }

  setAnimationState(state: string): void {
    this.currentAnimationState = state;
  }

}
