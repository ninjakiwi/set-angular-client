import { Component, OnInit } from '@angular/core';
import { GameServerService } from '../game-server.service';
import { GameStateService } from '../game-state.service';

@Component({
  selector: 'app-game-finished',
  templateUrl: './game-finished.component.html',
  styleUrls: ['../overlay/overlay.children.css']
})
export class GameFinishedComponent implements OnInit {
  mistakeMapping:
      {[k: string]: string} = {'=0': 'no mistakes', '=1': '1 mistake', 'other': '# mistakes'};

  constructor(
    public gameStateService: GameStateService,
    private gameServerService: GameServerService
    ) { }

  ngOnInit(): void {
  }

  getPosition(): number {
    for (const [index, score] of this.gameStateService.gameOverScores.entries()) {
      if (score.userId === this.gameStateService.userId) {
        return index + 1;
      }
    }
    return -1;
  }

  restartGame(): void {
    this.gameStateService.releaseOverlayLock();
    if (this.gameStateService.getGameState() === 'game-over') {
      this.gameServerService.restartGame();
    }
  }

  backToGameSelect(): void {
    this.gameStateService.releaseOverlayLock();
    this.gameServerService.leaveGame();
  }

  clipName(name: string): string {
    if (name.length > 7) {
      return name.slice(0, 7) + '..';
    }
    return name;
  }

}
