import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GameServerService } from '../game-server.service';
import { GameStateService } from '../game-state.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-find-game',
  templateUrl: './find-game.component.html',
  styleUrls: ['../overlay/overlay.children.css']
})
export class FindGameComponent implements OnInit {
  public showNameWarning = false;
  public username: string = '';
  joinGameForm = new FormGroup({
    gameName: new FormControl('')
  });

  constructor(
    public gameStateService: GameStateService,
    private gameServerService: GameServerService
  ) { }

  ngOnInit(): void {
    this.username = this.gameStateService.getUserName();
  }

  startGame(gameName: string): void {
    if (this.username === '') {
      this.showNameWarning = true;
      return;
    }
    this.showNameWarning = false;
    this.gameServerService.setUsername(this.username);
    this.gameServerService.joinGame(gameName);
    this.joinGameForm.reset();
  }

}
