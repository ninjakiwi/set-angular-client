export interface Score {
  userId: string,
  userName: string,
  sets: [string[]],
  mistakes: number
}
