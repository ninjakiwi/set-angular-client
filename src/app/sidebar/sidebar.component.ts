import { Component, OnInit } from '@angular/core';
import { GameServerService } from '../game-server.service';
import { GameStateService } from '../game-state.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  public showSidebar: boolean = false;

  constructor(
    public gameStateService: GameStateService,
    private gameServerService: GameServerService
  ) { }

  ngOnInit(): void {
  }

  leaveGame(): void {
    this.gameServerService.leaveGame();
  }

  toggleSidebar(): void {
    this.showSidebar = !this.showSidebar;
  }

}
