import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: "placing"})
export class PlacingPipe implements PipeTransform {
    transform(value: number): string {
        const valueAsString = value.toString();
        if (valueAsString.length < 1 || value < 1) { return "last"; }
        let suffix = "";
        switch (valueAsString[valueAsString.length - 1]) {
            case "1":
                suffix = "st";
                break;
            case "2":
                suffix = "nd";
                break;
            case "3":
                suffix = "rd";
                break;
            default:
                suffix = "th";
                break;
        }
        // Special cases
        if (value >= 11 && value <= 13) {
            suffix = "th";
        }
        return value + suffix;
    }
}