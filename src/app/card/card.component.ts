import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  public aspectRatio: number = 1.45;
  public xIndex: number = 0;
  public yIndex: number = 0;

  @Input() definition: string = "";

  constructor() {
  }

  ngOnInit(): void {
    const color = Number(this.definition[0]);
    const shading = Number(this.definition[1]);
    const number = Number(this.definition[2]);
    const shape = Number(this.definition[3]);

    // locate this card inside the spritesheet
    this.xIndex = (color * 3 + shading);
    this.yIndex = (number * 3 + shape);
  }

}
