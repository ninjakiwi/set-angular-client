import { animate, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { GameStateService } from './game-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('onAppearDisappear', [
      transition(':enter', [
        style({
          opacity: 0,
          gridTemplateRows: '1fr 0.2fr',
        }),
        animate('400ms ease-out', style({
          opacity: 1,
          gridTemplateRows: '1fr 0fr',
        })),
      ]),
      transition(':leave', [
        style({
          opacity: 1,
          gridTemplateRows: '1fr 0fr',
        }),
        animate('400ms ease-out', style({
          opacity: 0,
          gridTemplateRows: '1fr 0.2fr',
        })),
      ])
    ])
  ]
})
export class AppComponent {
  title = 'set-game-angular-client';
  constructor(public gameStateService: GameStateService) { }
}
